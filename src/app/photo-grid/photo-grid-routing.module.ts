import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PhotoGridComponent } from './photo-grid.component';


const routes: Routes = [
  {path: '', component: PhotoGridComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)]
})
export class PhotoGridRoutingModule { }
