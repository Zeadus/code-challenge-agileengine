import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatButtonModule, MatDialogModule } from '@angular/material';
import { ClipboardModule } from 'ngx-clipboard';
import { NgxImageZoomModule } from 'ngx-image-zoom';
import { PhotoGridRoutingModule } from './photo-grid-routing.module';

import { PhotoGridComponent } from './photo-grid.component';
import { PhotoDetailDialogComponent } from './shared/components/photo-detail-dialog/photo-detail-dialog.component';
import { PhotoService } from './shared/services/photos.service';

@NgModule({
  imports: [
    PhotoGridRoutingModule,
    CommonModule,
    FlexLayoutModule,
    MatButtonModule,
    MatDialogModule,
    NgxImageZoomModule,
    ClipboardModule
  ],
  exports: [],
  declarations: [
    PhotoGridComponent,
    PhotoDetailDialogComponent
  ],
  entryComponents: [PhotoDetailDialogComponent],
  providers: [PhotoService],
})
export class PhotoGridModule { }
