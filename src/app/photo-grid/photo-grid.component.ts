import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { PhotoDetailDialogComponent } from './shared/components/photo-detail-dialog/photo-detail-dialog.component';
import { PhotoService } from './shared/services/photos.service';

@Component({
  selector: 'app-photo-grid',
  templateUrl: './photo-grid.component.html',
  styleUrls: ['./photo-grid.component.css']
})

export class PhotoGridComponent implements OnInit {
  constructor(
    private photoService: PhotoService,
    public dialog: MatDialog
    ) { }
  currentPage = 0;
  pageCount = 1;
  hasMore = false;
  images = [];

  ngOnInit() {
    this.getImages();
  }

  getImages() {
    this.currentPage++;
    this.photoService.getImages(this.currentPage).subscribe((resp: any) => {
      this.images = this.images.concat(resp.pictures);
      this.hasMore = resp.hasMore;
      this.pageCount = resp.pageCount;
    });
  }

  imageDetails(image) {
    const imageDialog = this.dialog.open(PhotoDetailDialogComponent, {
      data: {selectedImage: image, currentPage: this.currentPage, pageCount: this.pageCount, images: this.images},
      disableClose: true,
      autoFocus: false
    });
  }
}
