import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Config } from 'src/app/config/config';
import { Observable } from 'rxjs';
@Injectable()
export class PhotoService {
  constructor(
    private http: HttpClient
  ) { }
  public getImages(page: number) {
    return this.http.get(Config.API_URL + `/images?page=${page}`);
  }

  public getImageDetails(id) {
    return this.http.get(Config.API_URL + `/images/${id}`);
  }

}
