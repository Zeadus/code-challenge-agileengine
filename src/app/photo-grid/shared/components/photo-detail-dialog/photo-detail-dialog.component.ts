import { Component, HostListener, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { PhotoService } from '../../services/photos.service';

@Component({
  selector: 'app-photo-detail-dialog',
  templateUrl: './photo-detail-dialog.component.html',
  styleUrls: ['./photo-detail-dialog.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class PhotoDetailDialogComponent implements OnInit {
  selectedImage;
  currentImageData;
  currentPage;
  images;
  @HostListener('document:keydown', ['$event'])
    keyEvent(event: KeyboardEvent): any {
        if (event.key === 'ArrowLeft') {
          this.prevImage();
        } else if (event.key === 'ArrowRight') {
          this.nextImage();
        }
    }

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public photoService: PhotoService
  ) {}


  ngOnInit(): void {
    // Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    // Add 'implements OnInit' to the class.
    this.images = [... this.data.images];
    this.selectedImage = this.images.findIndex(i => i.id === this.data.selectedImage);
    this.currentPage = this.data.currentPage;
    this.getImageDetails();
  }

  getImageDetails() {
    this.currentImageData = null;
    this.photoService.getImageDetails(this.images[this.selectedImage].id).subscribe((data: any) => {
      this.currentImageData = data;
      console.log(this.currentImageData);
    });
  }

  nextImage() {
    if (!this.images[this.selectedImage + 1]) {
      this.currentPage++;
      this.photoService.getImages(this.currentPage).subscribe((resp: any) => {
        this.images = this.images.concat(resp.pictures);
        this.selectedImage++;
        this.getImageDetails();
      });
    } else {
      this.selectedImage++;
      this.getImageDetails();
    }
  }

  prevImage() {
    if (!this.images[this.selectedImage - 1]) {
      this.selectedImage = this.images.length - 1;
    } else {
      this.selectedImage --;
    }

    this.getImageDetails();
  }

}
