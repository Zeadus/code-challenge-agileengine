import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PhotoGridComponent } from './photo-grid/photo-grid.component';


const routes: Routes = [
  {
    path: '', children: [
      {
        path: 'photos',
        loadChildren: './photo-grid/photo-grid.module#PhotoGridModule'
      }
    ],
  },
  {
    path: '**', redirectTo: 'photos'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
