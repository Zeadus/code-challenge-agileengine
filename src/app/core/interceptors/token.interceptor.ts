import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse, HttpClient } from '@angular/common/http';
import { Observable, Subject, throwError } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';
import { Config } from 'src/app/config/config';

@Injectable({
  providedIn: 'root'
})
export class TokenInterceptor implements HttpInterceptor {
  private refreshSubject: Subject<any> = new Subject<any>();

  constructor(
    private http: HttpClient
  ) {}

  getNewToken() {
    return this.http.post(Config.API_URL + '/auth', {apiKey: Config.API_KEY}).pipe(map((resp: any) => {
      localStorage.setItem('token', resp.token);
      return resp;
    }));
  }

  ifTokenExpired() {
    this.refreshSubject.subscribe({
      complete: () => {
        this.refreshSubject = new Subject<any>();
      }
    });
    if (this.refreshSubject.observers.length === 1) {
      // Hit refresh-token API passing the refresh token stored into the request
      // to get new access token and refresh token pair
      this.getNewToken().subscribe(this.refreshSubject);
    }
    return this.refreshSubject;
  }

  setHeader(req) {
    const token: string = localStorage.getItem('token');
    const request = req.clone({
      setHeaders: {
        authorization: `Bearer ${ token }`
      }
    });
    return request;
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (req.url.indexOf('auth') !== -1) {
      return next.handle(req);
    }


    const request = this.setHeader(req);

    return next.handle(request).pipe(
      catchError((err: HttpErrorResponse) => {

        if (err.status === 401) {
          return this.ifTokenExpired().pipe(
            switchMap(() => {
              return next.handle(this.setHeader(req));
            })
          );
        } else {
          return throwError( err );
        }
      })
    );
  }

}
