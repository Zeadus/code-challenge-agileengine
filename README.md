AgileEngine Angular 8 Code Challenge

Time to develop was a bit more than 3 hours (Target completion time was 2 hours)

The challenge was developed using:
- Angular Material
- ngx-image-zoom@0.4.2
- ngx-clipboard (Not completely necessary but copying to clipboard without an external library takes more time if you want to make it look pretty using a reusable function or directives)

To run the project: 

- Install dependencies using "npm install"
- Run "npm start"
- Open localhost:4200/photos to see the project running
